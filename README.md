Provides basic [Base64](https://en.wikipedia.org/wiki/Base64) Encode and Decode functionality.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
